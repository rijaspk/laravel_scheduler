<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg;
//use Pawlox\VideoThumbnail\Facade\VideoThumbnail;
use VideoThumbnail;
class FileUploadController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fileUpload');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'video' => 'required|max:2000|mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime'
        ]);
        if($validator->fails()){

            $error_message = $validator->errors()->toArray();

            return response()->json($error_message  , 400);
        }

        $uploadedVideo = $request->file('video');
        $fileName=$request->title.'_'.$uploadedVideo->getClientOriginalName();
        // Getting the extension of the file
        $extension = $uploadedVideo->getClientOriginalExtension();
        $path=storage_path('app/public/').$fileName;
        $thumb=$fileName.'png';
        $uploadStatus=Storage::putFileAs('/public',$uploadedVideo,$fileName);
        $thumb=VideoThumbnail::createThumbnail($path, storage_path('app/public'), $thumb,2,  $width = 640, $height = 480);
//        $uploadStatus=Storage::putFileAs('/public',$thumb,'thumb');
        return null;
//        $sec = 10;
//        $movie =  $uploadedVideo;
//        $thumbnail = 'thumbnail.png';
//
//        $ffmpeg = FFMpeg\FFMpeg::create();
//        $video = $ffmpeg->open($movie);
//        $frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($sec));
//        $frame->save($thumbnail);
//        Storage::putFileAs('/public',$frame,$thumbnail);


//        echo '<img src="'.$thumbnail.'">';
//      return $uploadStatus;
        if ($uploadStatus) {
            return response()->json('upload success', 200);
        }
        // Else, return error 400
        else {
            return response()->json('error', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
