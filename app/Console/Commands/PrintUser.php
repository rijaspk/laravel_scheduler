<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PrintUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'printUser{username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Print User name ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('username');
        $this->line('Hello '.$this->argument('user'));
    }
}
