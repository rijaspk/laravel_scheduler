<?php

namespace App\Console\Commands;
use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendRegUsersCountMail;
class RegisterdUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registerd:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of registered users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $totalusers=DB::table('users')->whereRaw('Date(created_at) = CURDATE()')
            ->count();
        Mail::to('rijaspk5195@gmail.com')->send(new SendRegUsersCountMail($totalusers));


    }
}
