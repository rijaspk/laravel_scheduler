@extends('layouts.app')
@section('content')

    <form id="upload-widget" method="post" action="{{url('/dropzone')}}" class="dropzone" enctype="multipart/form-data" >
        {!! csrf_field() !!}
        <div class="fallback">
            <input name="file" type="file" />
        </div>
    </form>




    @push('bodyscripts')

        <script type="text/javascript">
            Dropzone.options.uploadWidget = {
                paramName: 'uploadfile',
                maxFilesize: 5, // MB
                maxFiles: 2,
                acceptedFiles: "image/jpeg, image/png, image/jpg,video/avi,video/mpeg,video/mp4,video/quicktime",
                addRemoveLinks: true,
                init: function() {
                    this.on("uploadprogress", function(file, progress) {
                        console.log("File progress", progress);
                    });
                }
            };

        </script>
@endpush
@endsection
